'''
Created on Jun 19, 2013

@author: Prasoon Mp
'''

#class DataStore:
class myDict(dict):
    def add(self, key, value):
        self[key] = value    

class XMLParser:
    def __init__(self):
        self.data=""
        self.pointer=0
    def readFile(self,filename):
        self.filehandle=open(filename,"r")
        self.data=self.filehandle.read()
    def printFile(self):
        print self.data
    def parseError(self):
        print "Error in parsing: Killing instance"
        exit()
        
    
    xmlDict = myDict()
    #print(xmlDict)
    def isValidTag(self,string):
        subTag=""
        tagType=""
        #print string[-2:]
        string=string.replace(" ", "")
        if string[:1]=="<":
            tagType="startTag"
        if string[:2]=="</":
            tagType="endTag"
        if string[:5]=="<?xml":
            tagType="startRootTag"
        if string[-2:]=="/>":
            tagType="emptyTag"
        if string[:5]=="</xml":
            tagType="endRootTag"
            
        return tagType
  
    def getTagName(self,tag):
        tagName=""
        for x in tag:
            if x!="<" and x!="/" and x!=">":
                tagName+=x
            if x==" ":
                break
        return tagName
                
        
    #def getAttribute(self,tag):
        
#    def readValue(self):


    def recursiveXml(self):
        tag=""
        content=""
        flag=False

        for c in self.data:
            subXmlDict = myDict()
            if c=="\n" or c=="\r":
                continue
            
            if c=='<' or flag==True:
                flag=True
                tag+=c
                if content!="":
                    #print content
                    self.xmlDict=self.nextedDict(self.xmlDict, content)
                    
                content=""
            if c=='>' or flag==False:
                if tag!="":
                    #print tag
                    #print self.getTagName(tag)
                    #print self.isValidTag(tag)
                    if self.isValidTag(tag)=="startRootTag":
                        self.xmlDict.add(self.getTagName(tag),"")
                        
                    if self.isValidTag(tag)=="startTag":
                        subXmlDict.add(self.getTagName(tag),"")                       
                        self.xmlDict=self.nextedDict(self.xmlDict, subXmlDict)
                        
                    if self.isValidTag(tag)=="emptyTag":
                        subXmlDict.add(self.getTagName(tag),"    ")                       
                        self.xmlDict=self.nextedDict(self.xmlDict, subXmlDict)
                        
                    if self.isValidTag(tag)=="endTag":
                        self.xmlDict=self.updateDict(self.xmlDict, self.getTagName(tag),"    ")
                        
                    
                flag=False
                
                tag=""
            if c!='<' and c!='>' and flag==False:
                content+=c
   
        print(self.xmlDict)
        self.iterateDict(self.xmlDict)
       
    def nextedDict(self, Dict,subDict):  
        for Key, Value in Dict.items():
            
            if isinstance(Value, dict):
                self.nextedDict(Value,subDict)
            elif Value=="":
                Dict[Key]=subDict
                
            elif isinstance(subDict, dict):
                    for k,v in subDict.iteritems():
                        Dict[k]=v
        return Dict
                
    def updateDict(self, Dict,tag,content):  
        for Key, Value in Dict.iteritems():
            
            if Key==tag and Value=="":
                Dict[Key]=content
            #print Key, "=", Dictionary[Key]    
            if isinstance(Value, dict):
                self.updateDict(Value,tag,content)
        return Dict
    
    def iterateDict(self, Dict):  
        for Key, Value in Dict.iteritems():     
            print Key, "=", Value    
            if isinstance(Value, dict):
                self.iterateDict(Value)
        


p=XMLParser()
p.readFile("myxml.xml")
p.recursiveXml()
example_dict = { 'key1' : 'value1',
                     'key2' : 'value2',
                     'key3' : { 'key3a': 'value3a' },
                     'key4' : { 'key4a': { 'key4aa': 'value4aa',
                                           'key4ab': 'value4ab',
                                           'key4ac': 'value4ac'},
                                'key4b': 'value4b'}
                   }
#p.iterateDict(example_dict)
